from django.urls import path
from .views import UserDetailAPIView, UserCreateAPIView, UserUpdateAPIView
from rest_framework.authtoken import views

urlpatterns = [
    path('<int:id>/', UserDetailAPIView.as_view(), name='api_user_detail_url'),
    path('create/', UserCreateAPIView.as_view(), name='api_user_create_url'),
    path('token-auth/', views.obtain_auth_token, name='api_token_auth_url'),
    path('<int:id>/update/', UserUpdateAPIView.as_view(), name='api_user_update_url'),
]
