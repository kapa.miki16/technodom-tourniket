from rest_framework.permissions import BasePermission


class IsOwner(BasePermission):
    message = 'It is not your account'

    def has_object_permission(self, request, view, obj):
        return obj == request.user


class IsOwnerTeam(BasePermission):
    message = 'You are not part of a team'

    def has_object_permission(self, request, view, obj):
        if request.user in obj.users.all():
            return True
        return obj == request.user


class IsOwnerProfile(BasePermission):
    message = 'It is not your profile'

    def has_object_permission(self, request, view, obj):
        if obj.user == request.user:
            return True
        return False