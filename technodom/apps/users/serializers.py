from rest_framework import serializers
from .models import User
from rest_framework.validators import ValidationError, UniqueValidator


class UserDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            'id',
            'username',
            'first_name',
            'last_name',
            'father_name',
            'email',
            'address',
            'phone',
            'image'
        )



class UserCreateSerializer(serializers.ModelSerializer):
    first_name = serializers.CharField(label="First name")
    last_name = serializers.CharField(label="Last name")
    father_name = serializers.CharField(label="Father name")
    phone = serializers.IntegerField(min_value=10)
    address = serializers.CharField(label="Address")
    image = serializers.ImageField()
    email = serializers.EmailField(
        required=True,
        label='Email',
        validators=[UniqueValidator(queryset=User.objects.all())])
    password = serializers.CharField(label='Password', min_length=8, write_only=True)
    password2 = serializers.CharField(label='Confirm Password', write_only=True)

    class Meta:
        model = User
        fields = (
            'username',
            'email',
            'password',
            'password2',
            'first_name',
            'last_name',
            'father_name',
            'phone',
            'address',
            'image',
        )

    def create(self, validated_data):
        username = validated_data['username']
        email = validated_data['email']
        password = validated_data['password']
        first_name = validated_data['first_name']
        last_name = validated_data['last_name']
        father_name = validated_data['father_name']
        phone = validated_data['phone']
        address = validated_data['address']
        image = validated_data['image']
        user = User(
            username=username,
            email=email,
            first_name=first_name,
            last_name=last_name,
            father_name=father_name,
            phone=phone,
            address=address,
            image=image
        )
        if password != validated_data['password2']:
            raise ValidationError('Passwords not match!')
        else:
            user.set_password(password)
            user.save()
        return validated_data



class UserUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            'username',
            'email',
            'first_name',
            'last_name',
            'father_name',
            'phone',
            'address',
            'image',
        )