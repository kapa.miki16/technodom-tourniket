from django.db import models
from django.contrib.auth.models import AbstractUser



class User(AbstractUser):
    image = models.ImageField(upload_to='user_images')
    father_name = models.CharField(max_length=50)
    phone = models.IntegerField(default=0, unique=True)
    address = models.CharField(max_length=100)

