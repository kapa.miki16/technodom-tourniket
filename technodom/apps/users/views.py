from .models import User

from rest_framework.generics import (
    RetrieveAPIView,
    CreateAPIView,
    RetrieveUpdateAPIView
)
from .serializers import (
    UserDetailSerializer,
    UserCreateSerializer,
    UserUpdateSerializer
)
from rest_framework.permissions import IsAuthenticated
from .permissions import IsOwner



class UserDetailAPIView(RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserDetailSerializer
    lookup_field = 'id'


class UserCreateAPIView(CreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserCreateSerializer


class UserUpdateAPIView(RetrieveUpdateAPIView):
    queryset = User.objects.all()
    serializer_class = UserUpdateSerializer
    lookup_field = 'id'
    permission_classes = (IsAuthenticated, IsOwner)